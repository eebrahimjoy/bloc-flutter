import 'dart:async';

import 'package:seip/authentication/authentication_event.dart';

class AuthenticationBloc {
  int _counter = 0;
  final _counterStateController = StreamController<int>();
  final _counterEventController = StreamController<AuthenticationEvent>();

  StreamSink<int> get _inCounnter => _counterStateController.sink;

  Stream<int> get counnter => _counterStateController.stream;

  Sink<AuthenticationEvent> get counnterEventSink =>
      _counterEventController.sink;

  AuthenticationBloc() {
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(AuthenticationEvent event) {
    if (event is IncrementEvent) {
      _counter++;
    } else {
      _counter--;
    }
    _inCounnter.add(_counter);
  }

  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}
